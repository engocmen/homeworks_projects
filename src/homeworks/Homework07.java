package homeworks;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Homework07 {
    public static void main(String[] args) {


        System.out.println("\n--------------------Task1-----------------------");

        List<Integer> nums = new ArrayList<>();
        nums.add(10);
        nums.add(23);
        nums.add(67);
        nums.add(23);
        nums.add(78);

        System.out.println(nums.get(3));
        System.out.println(nums.get(0));
        System.out.println(nums.get(2));
        System.out.println(nums);

        System.out.println("\n--------------------Task2-----------------------");

        ArrayList<String> colors = new ArrayList<>(Arrays.asList("Blue", "Brown", "Red", "White", "Black", "Purple"));

        System.out.println(colors.get(1));
        System.out.println(colors.get(3));
        System.out.println(colors.get(5));
        System.out.println(colors);

        System.out.println("\n--------------------Task3-----------------------");

        ArrayList <Integer> nums1 = new ArrayList<>(Arrays.asList(23, -34, -56, 0, 89, 100));

        System.out.println(nums1);
        Collections.sort(nums1);
        System.out.println(nums1);

        System.out.println("\n--------------------Task4-----------------------");

        ArrayList<String> city = new ArrayList<>(Arrays.asList("Istanbul", "Berlin", "Madrid", "Paris"));

        System.out.println(city);
        Collections.sort(city);
        System.out.println(city);

        System.out.println("\n--------------------Task5-----------------------");

        ArrayList<String> marvel = new ArrayList<>(Arrays.asList("Spider Man", "Iron Man", "Black Panter", "Deadpool", "Captain America"));
        boolean containsWolverine = false;
        System.out.println(marvel);
        for (String s : marvel) {
            if (s.contains("Wolwerine")) containsWolverine = true;
        }
        System.out.println(containsWolverine);

        System.out.println("\n--------------------Task6-----------------------");

        ArrayList<String> marvel1 = new ArrayList<>(Arrays.asList("Hulk", "Black Widow", "Captain America", "Iron Man"));
        boolean containsHulk = false;
        boolean containsIron = false;
        Collections.sort(marvel1);
        System.out.println(marvel1);
        for (String s : marvel1) {
            if (s.contains("Hulk")) containsHulk = true;
            if(s.contains("Iron Man")) containsIron = true;
        }
        System.out.println(containsHulk&&containsIron);

        System.out.println("\n--------------------Task7-----------------------");

        ArrayList<Character> chars = new ArrayList<>(Arrays.asList('A', 'x', '$', '%', '9', '*', '+', 'F', 'G'));

        System.out.println(chars);
        for (Character aChar : chars) {
            System.out.println(aChar);
        }

        System.out.println("\n--------------------Task8-----------------------");

        ArrayList<String> tools = new ArrayList<>(Arrays.asList("Desk", "Laptop", "Mouse", "Monitor", "Mouse-Pad", "Adapter"));

        System.out.println(tools);
        Collections.sort(tools);
        System.out.println(tools);

        int countM = 0, countAE = 0;
        for (String tool : tools) {
            if (tool.toLowerCase().startsWith("m")) countM++;
            if (tool.toLowerCase().contains("a") || tool.toLowerCase().contains("e")) continue;
                else countAE++;}

        System.out.println(countM + "\n" + countAE);

        System.out.println("\n--------------------Task9-----------------------");

        ArrayList<String> kitchen = new ArrayList<>(Arrays.asList("Plate", "spoon", "fork", "Knife", "cup", "Mixer"));

        System.out.println(kitchen);
        int countUpper=0, countLower=0, countP=0, countStartEndP=0;
        for (String s : kitchen) {
            if (Character.isUpperCase(s.charAt(0))) countUpper++;
            if (Character.isLowerCase(s.charAt(0))) countLower++;
            if (s.toLowerCase().contains("p")) countP++;
            if (s.toLowerCase().startsWith("p") || s.toLowerCase().endsWith("p")) countStartEndP++;
        }

        System.out.println("Elements starts with uppercase = " + countUpper);
        System.out.println("Elements starts with lowercase = " + countLower);
        System.out.println("Elements having P or p = " + countP);
        System.out.println("Elements starting or ending with P or p = " + countStartEndP);

        System.out.println("\n--------------------Task10-----------------------");

        ArrayList<Integer> nums2 = new ArrayList<>(Arrays.asList(3, 5, 7, 10, 0, 20, 17, 10, 23, 56, 78));

        System.out.println(nums2);
        int divide = 0, even = 0, odd = 0, range = 0;
        for (Integer num : nums2) {
            if (num%10==0) divide++;
            if (num>15 && num%2==0) even++;
            if(num<20 && num%2!=0) odd++;
            if(num<15 || num>50) range++;
        }
        System.out.println("Elements that can be divided by 10 = " + divide);
        System.out.println("Elements that are even and greater than 15 = " + even);
        System.out.println("Elements that are odd and less than 20 = " + odd);
        System.out.println("Elements that are less than 15 or greater than 50 = " + range);

        System.out.println("\n--------------------EndOfTask-----------------------");


    }
}
