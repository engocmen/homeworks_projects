package homeworks;

import java.util.stream.Stream;

public class Homework14 {

    //      System.out.println("\n--------------------Task1-----------------------");

    public static void fizzBuzz1(int n) {
        for (int i = 1; i <= n; i++) {
            if (i % 3 == 0 && i % 5 == 0) {
                System.out.println("FizzBuzz");
            } else if (i % 3 == 0) {
                System.out.println("Fizz");
            } else if (i % 5 == 0) {
                System.out.println("Buzz");
            } else {
                System.out.println(i);
            }
        }
    }

    //      System.out.println("\n--------------------Task2-----------------------");

    public static String fizzBuzz2(int num){
        if (num%15==0) return "FizzBuzz";
        if (num%3==0) return "Fizz";
        if (num%5==0) return "Buzz";
        return ""+num;
    }

    //      System.out.println("\n--------------------Task3-----------------------");

    public static int findSumNumbers(String str){
        String temp = "";
        int total = 0;
        for (int i = 0; i < str.length(); i++) {
            if (Character.isDigit(str.charAt(i))) temp += str.charAt(i);
            else if (!Character.isDigit(str.charAt(i))&&!temp.isEmpty()) {
                total += Integer.parseInt(temp);
                temp = "";
            }
        }if (!temp.isEmpty()) total += Integer.parseInt(temp);
        return total;
        }

    //      System.out.println("\n--------------------Task4-----------------------");

    public static int findBiggestNumber(String str){
        String temp1 = "";
        String temp2 = "";
        int greatest = 0;
        for (int i = 0; i < str.length(); i++) {
            if (Character.isDigit(str.charAt(i))) temp1 += str.charAt(i);
            if (!Character.isDigit(str.charAt(i))&&!temp1.isEmpty()&&temp2.isEmpty()) {
                temp2 = temp1;
                temp1 = "";
            } if (!Character.isDigit(str.charAt(i))&&!temp1.isEmpty()&&!temp2.isEmpty()) {
                temp2 = (Integer.parseInt(temp1) > Integer.parseInt(temp2) ? temp1 : temp2);
                temp1 = "";
                greatest = Integer.parseInt(temp2);
            }
        }if (!temp1.isEmpty()&&!temp2.isEmpty()&&Integer.parseInt(temp1) > Integer.parseInt(temp2)) greatest = Integer.parseInt(temp1);
        if(!temp1.isEmpty()&&temp2.isEmpty()) greatest = Integer.parseInt(temp1);
        return greatest;
    }



    //      System.out.println("\n--------------------Task5-----------------------");

    public static String countSequenceOfCharacters(String s) {
        if (s.isEmpty())
            return "";
        String result = "";
        int count = 1;
        for (int i = s.length()-1; i >= 0; i--) {
            if (i>0&&s.charAt(i)==s.charAt(i-1)) {
                count++;}
            else {
                result =""+count+s.charAt(i)+result;
                count = 1;
            }} return result;}

   //     System.out.println("\n--------------------EndOfTasks-----------------------");





    public static void main(String[] args) {

        System.out.println("\n--------------------Task1-----------------------");

        Homework14.fizzBuzz1(3);
        System.out.println("--------------------");
        Homework14.fizzBuzz1(5);
        System.out.println("--------------------");
        Homework14.fizzBuzz1(18);

        System.out.println("\n--------------------Task2-----------------------");

        System.out.println(Homework14.fizzBuzz2(0));
        System.out.println(Homework14.fizzBuzz2(1));
        System.out.println(Homework14.fizzBuzz2(3));
        System.out.println(Homework14.fizzBuzz2(5));
        System.out.println(Homework14.fizzBuzz2(15));

        System.out.println("\n--------------------Task3-----------------------");

        System.out.println(Homework14.findSumNumbers("abc$"));
        System.out.println(Homework14.findSumNumbers("a1b4c  6#"));
        System.out.println(Homework14.findSumNumbers("ab110c045d"));
        System.out.println(Homework14.findSumNumbers("525"));

        System.out.println("\n--------------------Task4-----------------------");

        System.out.println(Homework14.findBiggestNumber("abc$"));
        System.out.println(Homework14.findBiggestNumber("a1b4c  6#"));
        System.out.println(Homework14.findBiggestNumber("ab110c045d"));
        System.out.println(Homework14.findBiggestNumber("525"));

        System.out.println("\n--------------------Task5-----------------------");

        System.out.println(Homework14.countSequenceOfCharacters(""));
        System.out.println(Homework14.countSequenceOfCharacters("abc"));
        System.out.println(Homework14.countSequenceOfCharacters("abbcca"));
        System.out.println(Homework14.countSequenceOfCharacters("aaAAa"));

        System.out.println("\n--------------------EndOfTasks-----------------------");

    }
}
