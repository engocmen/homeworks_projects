package homeworks;

import java.util.Arrays;

public class Homework09 {

    //      System.out.println("\n--------------------Task1-----------------------");

    public static String thereIsNoDuplicateNumbers(int[] nums){
        String result = "There is no duplicates";
        for (int i = 0; i < nums.length; i++) {
            for (int j = i+1; j < nums.length; j++) {
                if (nums[i]==nums[j]) return "" +nums[i];}}
    return result;}

    //      System.out.println("\n--------------------Task2-----------------------");

    public static String thereIsNoDuplicates(String[] words){
        String result = "There is no duplicates";
        for (int i = 0; i < words.length; i++) {
            for (int j = i+1; j < words.length; j++) {
                if (words[i].equalsIgnoreCase(words[j])) return "" +words[i];}}
        return result;}

    //      System.out.println("\n--------------------Task3-----------------------");

    public static String thereIsNoMultipleDuplicateNumbers(int[] nums){
        String result = "There is no duplicates";
        String answer = "";
        for (int i = 0; i < nums.length; i++) {
            for (int j = i+1; j < nums.length; j++) {
                if (nums[i]==nums[j]&&!answer.contains(""+nums[i])) answer += nums[i] + "\n";}}
        if (!answer.isEmpty()) return answer.substring(0, answer.length()-1);
        else return result;}

    //      System.out.println("\n--------------------Task4-----------------------");

    public static String thereIsNoMultipleDuplicateWords(String[] words){
        String result = "There is no duplicates";
        String answer = "";
        for (int i = 0; i < words.length; i++) {
            for (int j = i+1; j < words.length; j++) {
if (words[i].equalsIgnoreCase(words[j])&&!answer.toLowerCase().contains(words[i].toLowerCase())) answer += words[i] + "\n";}}
        if (!answer.isEmpty()) return answer.substring(0, answer.length()-1);
        else return result;}

    //      System.out.println("\n--------------------Task5-----------------------");

    public static String[] reverseEachWord(String[] words){
        String[] str = new String[words.length];
        int count = 0;
        for (int i = words.length-1; i >= 0 ; i--) {
            str[count] = words[i];
            count++;}
    return str;}

    //      System.out.println("\n--------------------Task6-----------------------");

    public static String[] reverseEachWordSameSpot(String[] words){
        String[] str = new String[words.length];
        String eachWord = "";
        int count1 = 0, count2 = 0;
        for (String word : words) {
        for (int i = word.length()-1; i >= 0 ; i--) {
           eachWord += word.charAt(i);
            count1++;}
        str[count2] = eachWord;
        eachWord = "";
        count2++;}
        return str;}

    //      System.out.println("\n--------------------EndOfTask-----------------------");


}







