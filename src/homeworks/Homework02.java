package homeworks;

import java.util.Scanner;

public class Homework02 {
    public static void main(String[] args) {

        Scanner inputs = new Scanner(System.in);


        System.out.println("--------------------Task1------------------------");


        System.out.println("User please enter first number.");
        int num1 = inputs.nextInt();

        System.out.println("User please enter second number.");
        int num2 = inputs.nextInt();
        System.out.println("The number 1 entered by user is = " + num1);
        System.out.println("The number 2 entered by user is = " + num2);
        System.out.println("The sum of number 1 and 2 entered by user is = " + (num1 + num2));


        System.out.println("\n--------------------Task2------------------------");


        System.out.println("User please enter Two number.");
        int numb1 = inputs.nextInt(), numb2 = inputs.nextInt();


        System.out.println("The product of the given 2 numbers is: " + (numb1 * numb2));


        System.out.println("\n--------------------Task3------------------------");


        System.out.println("User please enter 2 floating numbers .");
        double numbe1 = inputs.nextDouble(), numbe2 = inputs.nextDouble();

        System.out.println("The sum of the given numbers is = " + (numbe1 + numbe2) + "\n" +
                "The product of the given numbers is = " + numbe1 * numbe2 + "\n" +
                "The subtraction of the given numbers is = " + (numbe1 - numbe2) + "\n" +
                "The division of the given numbers is = " + numbe1 / numbe2 + "\n" +
                "The remainder of the given numbers is = " + numbe1 % numbe2 + "\n");


        System.out.println("\n--------------------Task4------------------------");


        System.out.println("1. \t\t" + (-10 + 7 * 5) + "\n" +
                "2. \t\t" + ((72+24) % 24) + "\n" +
                "3. \t\t" + (10 + -3*9 / 9) + "\n" +
                "4. \t\t" + (5 + 18 / 3 * 3 - (6 % 3)));


        System.out.println("\n--------------------Task5------------------------");


        System.out.println("User please enter 2 numbers .");
        int number1 = inputs.nextInt(), number2 = inputs.nextInt();

        System.out.println("The average of the given numbers is: " + ((number1 + number2)/2));


        System.out.println("\n--------------------Task6------------------------");


        System.out.println("User please enter 5 numbers.");
        int nu1 = inputs.nextInt(), nu2 = inputs.nextInt(), nu3 = inputs.nextInt(),
                nu4 = inputs.nextInt(), nu5 = inputs.nextInt();

        System.out.println("The average of the given numbers is: " + ((nu1 + nu2 + nu3 + nu4 + nu5)/5));


        System.out.println("\n--------------------Task7------------------------");


        System.out.println("User please enter 3 numbers.");
        int n1 = inputs.nextInt(), n2 = inputs.nextInt(), n3 = inputs.nextInt();

        System.out.println("The " + n1 + " multiplied with " + n1 + " is = " + n1 * n1 + "\n" +
                "The " + n2 + " multiplied with " + n2 + " is = " + n2 * n2 + "\n" +
                "The " + n3 + " multiplied with " + n3 + " is = " + n3 * n3 + "\n");


        System.out.println("\n--------------------Task8------------------------");


        System.out.println("User please enter 1 side of the square.");
        int sqN1 = inputs.nextInt();

        System.out.println("Perimeter of the square = " + sqN1 * 4 + "\n" +
                "Area of the square = " + sqN1 * sqN1);


        System.out.println("\n--------------------Task9------------------------");


        int averageSalary = 90_000, numbYrs = 3;

        System.out.println("A Software Engineer in Test can earn $" + averageSalary * numbYrs +  " in "+ numbYrs + " years.");
        inputs.nextLine();

        System.out.println("\n--------------------Task10------------------------");


        System.out.println("User please enter your favorite book.");
        String favBook = inputs.nextLine();
        System.out.println("User please enter your favorite color.");
        String favColor = inputs.nextLine();
        System.out.println("User please enter your favorite number.");
        int favNum = inputs.nextInt();
        inputs.nextLine();

        System.out.println("User’s favorite book is: " + favBook + "\n" +
                "User’s favorite color is: " + favColor + "\n" +
                "User’s favorite number is: " + favNum);


        System.out.println("\n--------------------Task11------------------------");


        System.out.println("User please enter your first name.");
        String fName = inputs.nextLine();
        System.out.println("User please enter your last name.");
        String lName = inputs.nextLine();
        System.out.println("User please enter your age.");
        int age = inputs.nextInt();
        inputs.nextLine();
        System.out.println("User please enter your email.");
        String email = inputs.nextLine();
        System.out.println("User please enter your address.");
        String address = inputs.nextLine();
        System.out.println("User please enter your phone number.");
        String phone = inputs.nextLine();

        System.out.println("User who joined this program is " + fName + " " + lName + ". " + fName + "’s age is " +
                age + ". " + fName + "’s email \naddress is " + email + ", phone number is " + phone + ", and address \nis " +
                address + ".");
    }
}
