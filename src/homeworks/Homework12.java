package homeworks;

import java.util.Arrays;

public class Homework12 {



    //      System.out.println("\n--------------------Task1-----------------------");

    public static String noDigits(String str){
        return str.replaceAll("[\\d]", "");
    }

    //      System.out.println("\n--------------------Task2-----------------------");

    public static String noVowels(String str){
        return str.replaceAll("[AEIOUaeiou]", "");
    }

    //      System.out.println("\n--------------------Task3-----------------------");

    public static int sumOfDigits(String str){
        int sum = 0;
        for (int i = 0; i < str.length(); i++) {
            if (Character.isDigit(str.charAt(i))){
           sum += Integer.parseInt(""+str.charAt(i));
        }}return sum;
    }

    //      System.out.println("\n--------------------Task4-----------------------");

    public static boolean hasUpperCase(String str){
        for (int i = 0; i < str.length(); i++) {
            if (Character.isUpperCase(str.charAt(i))){
                return true;
            }}return false;
    }

    //      System.out.println("\n--------------------Task5-----------------------");

    public static int middleInt(int a, int b, int c){
        int[] nums = {a, b, c};
        Arrays.sort(nums);
        return nums[1];
    }

    //      System.out.println("\n--------------------Task6-----------------------");

    public static int[] no13(int[] nums){
        for (int i = 0; i < nums.length; i++) {
        if (nums[i]==13)nums[i]=0;
        }return nums;}

    //      System.out.println("\n--------------------Task7-----------------------");

    public static int[] arrFactorial(int[] nums){
        int temp = 1;
            for (int i = 0; i < nums.length; i++) {
                for (int j = 2; j <= nums[i]; j++) {
                    temp = temp * j;
                } nums[i] = temp;
                temp = 1;
            } return nums;
        }

    //      System.out.println("\n--------------------Task8-----------------------");

    public static String[] catagorizeCharacters(String str){
        return new String[]{str.replaceAll("[\\W^\\d]", ""), str.replaceAll("[\\D]", ""),
                str.replaceAll("[\\w\\d]", "")};
    }

    //      System.out.println("\n--------------------EndOfTasks-----------------------");




    public static void main(String[] args) {

             System.out.println("\n--------------------Task1-----------------------");

        System.out.println(Homework12.noDigits(""));
        System.out.println(Homework12.noDigits("Java"));
        System.out.println(Homework12.noDigits("123Hello"));
        System.out.println(Homework12.noDigits("123Hello World149"));
        System.out.println(Homework12.noDigits("123Tech456Global149"));

        System.out.println("\n--------------------Task2-----------------------");

        System.out.println(Homework12.noVowels(""));
        System.out.println(Homework12.noVowels("wyz"));
        System.out.println(Homework12.noVowels("JAVA"));
        System.out.println(Homework12.noVowels("125$"));
        System.out.println(Homework12.noVowels("TechGlobal"));

        System.out.println("\n--------------------Task3-----------------------");

        System.out.println(Homework12.sumOfDigits(""));
        System.out.println(Homework12.sumOfDigits("Java"));
        System.out.println(Homework12.sumOfDigits("John’s age is 29"));
        System.out.println(Homework12.sumOfDigits("$125.0"));

        System.out.println("\n--------------------Task4-----------------------");

        System.out.println(Homework12.hasUpperCase(""));
        System.out.println(Homework12.hasUpperCase("java"));
        System.out.println(Homework12.hasUpperCase("John's Age is 29"));
        System.out.println(Homework12.hasUpperCase("$125.0"));

        System.out.println("\n--------------------Task5-----------------------");

        System.out.println(Homework12.middleInt(1,1,1));
        System.out.println(Homework12.middleInt(1,2,2));
        System.out.println(Homework12.middleInt(5,5,8));
        System.out.println(Homework12.middleInt(5,3,5));
        System.out.println(Homework12.middleInt(-1,25,10));

        System.out.println("\n--------------------Task6-----------------------");

        int[] no1 = {1,2,3,4}, no2 = {13,2,3}, no3 = {13,13,13,13,13};
        System.out.println(Arrays.toString(Homework12.no13(no1)));
        System.out.println(Arrays.toString(Homework12.no13(no2)));
        System.out.println(Arrays.toString(Homework12.no13(no3)));

        System.out.println("\n--------------------Task7-----------------------");

        int[] arr1 = {1,2,3,4}, arr2 = {0,5}, arr3 = {5,0,6}, arr4 = {};
        System.out.println(Arrays.toString(Homework12.arrFactorial(arr1)));
        System.out.println(Arrays.toString(Homework12.arrFactorial(arr2)));
        System.out.println(Arrays.toString(Homework12.arrFactorial(arr3)));
        System.out.println(Arrays.toString(Homework12.arrFactorial(arr4)));

        System.out.println("\n--------------------Task8-----------------------");

        System.out.println(Arrays.toString(Homework12.catagorizeCharacters("")));
        System.out.println(Arrays.toString(Homework12.catagorizeCharacters("abc123$#%")));
        System.out.println(Arrays.toString(Homework12.catagorizeCharacters("12Ab$%3c%")));

        System.out.println("\n--------------------EndOfTasks-----------------------");





    }
}
