package homeworks;

import java.util.Map;
import java.util.TreeMap;

public class Homework16 {


    //      System.out.println("\n--------------------Task1-----------------------");

    public static void parseData(String str) {
        Map<String, String> map = new TreeMap<>();
        String[] words = str.split("[\\W]+");
        for (int i = 1; i < words.length; i++, i++ ) {
            map.put(words[i], words[i+1]);
        }
        System.out.println(map);
    }

    //      System.out.println("\n--------------------Task2-----------------------");

    public static double calculateTotalPrice1(Map<String, Integer> items){
        double applePrice = 2.00;
        double orangePrice = 3.29;
        double mangoPrice = 4.99;
        double pineapplePrice = 5.25;
        double totalPrice = 0.0;
        for (Map.Entry<String, Integer> item : items.entrySet()) {
            String key = item.getKey();
            int value = item.getValue();
            if (key.equals("Apple")) totalPrice += applePrice * value;
            if (key.equals("Orange")) totalPrice += orangePrice * value;
            if (key.equals("Mango")) totalPrice += mangoPrice * value;
            if (key.equals("Pineapple")) totalPrice += pineapplePrice * value;

        }return totalPrice;}

    //      System.out.println("\n--------------------Task3-----------------------");

    public static double calculateTotalPrice2(Map<String, Integer> items){
        double applePrice = 2.00;
        double appleDiscount = 0;
        double orangePrice = 3.29;
        double mangoPrice = 4.99;
        double mangoDiscount = 0;
        double totalPrice = 0.0;
        for (Map.Entry<String, Integer> item : items.entrySet()) {
            String key = item.getKey();
            int value = item.getValue();
            if (key.equals("Apple")){
                appleDiscount = ((value/2)*(applePrice/2));
                totalPrice += (applePrice * value)-appleDiscount;
            }
            if (key.equals("Orange")) totalPrice += orangePrice * value;
            if (key.equals("Mango")){
                mangoDiscount = ((value/4)*(mangoPrice));
                totalPrice += (mangoPrice * value)-mangoDiscount;
            }

        }return totalPrice;}

    //      System.out.println("\n--------------------EndOfTasks-----------------------");



    public static void main(String[] args) {

        System.out.println("\n--------------------Task1-----------------------");

        Homework16.parseData("{104}LA{101}Paris{102}Berlin{103}Chicago{100}London");

        System.out.println("\n--------------------Task2-----------------------");

        TreeMap<String, Integer> items = new TreeMap<>();
        items.put("Apple", 3);
        items.put("Mango", 1);
        System.out.println(Homework16.calculateTotalPrice1(items));
        TreeMap<String, Integer> items1 = new TreeMap<>();
        items1.put("Apple", 2);
        items1.put("Pineapple", 1);
        items1.put("Orange", 3);
        System.out.println(Homework16.calculateTotalPrice1(items1));

        System.out.println("\n--------------------Task3-----------------------");

        TreeMap<String, Integer> items2 = new TreeMap<>();
        items2.put("Apple", 3);
        items2.put("Mango", 5);
        System.out.println(Homework16.calculateTotalPrice2(items2));
        TreeMap<String, Integer> items3 = new TreeMap<>();
        items3.put("Apple", 4);
        items3.put("Mango", 8);
        items3.put("Orange", 3);
        System.out.println(Homework16.calculateTotalPrice2(items3));

        System.out.println("\n--------------------EndOfTasks-----------------------");


    }
}
