package homeworks;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Homework15 {

    //      System.out.println("\n--------------------Task1-----------------------");

    public static int[] fibonacciSeries1(int n){
            int[] fibonacciSeries = new int[n];
            fibonacciSeries[0] = 0;
            fibonacciSeries[1] = 1;
            for (int i = 2; i < n; i++) {
                fibonacciSeries[i] = fibonacciSeries[i-1] + fibonacciSeries[i-2];
            }
            return fibonacciSeries;
        }

    //      System.out.println("\n--------------------Task2-----------------------");
//STILL NOT WORKING
    public static int fibonacciSeries2(int n){
        if (n <= 1) {
            return n;}
        int total = 1;
        for (int i = 0; i < n; i++) {
            total = i + (i-1);
        }
        return total;
    }

    //      System.out.println("\n--------------------Task3-----------------------");

    public static int[] findUniques(int[] array1, int[] array2){

        int[] result = new int[array1.length + array2.length];
        System.arraycopy(array1, 0, result, 0, array1.length);
        System.arraycopy(array2, 0, result, array1.length, array2.length);
        Arrays.sort(result);

        ArrayList<Integer> list = new ArrayList<>();
        for (int i = 0; i < result.length; i++) {
            if (i<result.length-1&&result[i]==result[i+1]){
                i++;
            } else if (i==result.length-1&&result[i]==result[i-1]) {
                continue;
            } else list.add(result[i]);
        }
        return list.stream().mapToInt(i->i).toArray();
    }

    //      System.out.println("\n--------------------Task4-----------------------");

    public static boolean isPowerOf3(int num){
        if (num <= 1) {
            return false;
        }
        while (num % 3 == 0) {
            num /= 3;
        }
        return num == 1;
    }

    //      System.out.println("\n--------------------Task5-----------------------");

//STILL NOT WORKING
    public static int firstDuplicate(int[] num){

        ArrayList<Integer>list = new ArrayList<>();
        for (int i = 0; i < num.length; i++) {
            for (int j = i+1; j < num.length; j++) {
                if (num[i]==num[j]) list.add(num[i]);
            }}
        return -1;
    }

    //      System.out.println("\n--------------------EndOfTasks-----------------------");






















    public static void main(String[] args) {

        System.out.println("\n--------------------Task1-----------------------");

        System.out.println(Arrays.toString(Homework15.fibonacciSeries1(3)));
        System.out.println(Arrays.toString(Homework15.fibonacciSeries1(5)));
        System.out.println(Arrays.toString(Homework15.fibonacciSeries1(7)));

        System.out.println("\n--------------------Task2-----------------------");

        System.out.println(Homework15.fibonacciSeries2(2));
        System.out.println(Homework15.fibonacciSeries2(4));
        System.out.println(Homework15.fibonacciSeries2(8));

        System.out.println("\n--------------------Task3-----------------------");

        System.out.println(Arrays.toString(Homework15.findUniques(new int[]{}, new int[]{})));
        System.out.println(Arrays.toString(Homework15.findUniques(new int[]{}, new int[]{1,2,3,2})));
        System.out.println(Arrays.toString(Homework15.findUniques(new int[]{1,2,3,4}, new int[]{3,4,5,5})));
        System.out.println(Arrays.toString(Homework15.findUniques(new int[]{8,9}, new int[]{9,8,9})));

        System.out.println("\n--------------------Task4-----------------------");

        System.out.println(Homework15.isPowerOf3(1));
        System.out.println(Homework15.isPowerOf3(2));
        System.out.println(Homework15.isPowerOf3(3));
        System.out.println(Homework15.isPowerOf3(81));

        System.out.println("\n--------------------Task5-----------------------");

        System.out.println(Homework15.firstDuplicate(new int[]{}));
        System.out.println(Homework15.firstDuplicate(new int[]{1}));
        System.out.println(Homework15.firstDuplicate(new int[]{1,2,2,3}));
        System.out.println(Homework15.firstDuplicate(new int[]{1,2,3,3,4,1}));

        System.out.println("\n--------------------EndOfTasks-----------------------");


    }
}
