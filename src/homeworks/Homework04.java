package homeworks;

import utilities.ScannerHelper;

import java.util.Scanner;

public class Homework04 {
    public static void main(String[] args) {
Scanner input = new Scanner(System.in);

        System.out.println("--------------------Task1------------------------");


        String name = ScannerHelper.getAName();
        System.out.println("The length of the name is = " + name.length());
        System.out.println("The first character in the name is = " + name.charAt(0));
        System.out.println("The last character in the name is = " + name.charAt(name.length()-1));
        System.out.println("The first 3 characters in the name are = " + name.substring(0,3));
        System.out.println("The last 3 characters in the name are = " + name.substring(name.length()-3));
        if(name.charAt(0)=='a'||name.charAt(0)=='A') System.out.println("You are in the club!");
        System.out.println("Sorry, you are not in the club");

        System.out.println("--------------------Task2------------------------");

        String address = ScannerHelper.getAnAddress();
        if(address.toLowerCase().contains("chicago")){
            System.out.println("You are in the club");}
        else if(address.toLowerCase().contains("des plaines")){
            System.out.println("You are welcome to join to the club");}
        else
            System.out.println("Sorry, you will never be in the club");

            System.out.println("--------------------Task3------------------------");

            System.out.println("Please enter 4 of your favorite colors");
            String color1=input.nextLine(),color2=input.nextLine(),color3=input.nextLine(),color4=input.nextLine();
            String colorAll=color1+color2+color3+color4;
            if(colorAll.toLowerCase().contains("green")&&colorAll.toLowerCase().contains("red"))
            {System.out.println("Green and red are in the list");}
            else if(colorAll.toLowerCase().contains("green"))
            {System.out.println("Green is in the list");}
        else if(colorAll.toLowerCase().contains("red"))
        {System.out.println("Red is in the list");}
            else System.out.println("Green and red are not in the list");

        System.out.println("--------------------Task4------------------------");

        String str = "Java is cool";
        System.out.println("The first word in the str is = " + str.substring(0,str.indexOf(" ")));
        System.out.println("The second word in the str is = " + str.substring(str.indexOf(" ")+1, str.lastIndexOf(" ")));
        System.out.println("The third word in the str is = " + str.substring(str.lastIndexOf(" ")+1));

        System.out.println("--------------------EndofTasks------------------------");

    }
}
