package homeworks;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Pattern;

public class Homework13 {


    //      System.out.println("\n--------------------Task1-----------------------");

    public static boolean hasLowerCase(String str){
        boolean answer = false;
        for (int i = 0; i < str.length(); i++) {
            if (Character.isLowerCase(str.charAt(i))) {
                answer = true;
            break;}
        }return answer;}

    //      System.out.println("\n--------------------Task2-----------------------");

    public static ArrayList<Integer> noZero(ArrayList<Integer> arr){
        for (int i = arr.size()-1; i >= 0; i--) {
            if (arr.get(i)==0) arr.remove(i);
        }return arr;}

    //      System.out.println("\n--------------------Task3-----------------------");

    public static int[][] numberAndSquare(int[] array) {
        int[][] result = new int[array.length][2];

        for (int i = 0; i < array.length; i++) {
            result[i][0] = array[i];
            result[i][1] = array[i] * array[i];
        }return result;
    }

    //      System.out.println("\n--------------------Task4-----------------------");

    public static boolean containsValue(String[] array, String value) {
        for (String str : array) {
            if (str.equals(value)) {
                return true;}}
        return false;}

    //      System.out.println("\n--------------------Task5-----------------------");

    public static String reverseSentence(String sentence) {
        String[] wor = sentence.split(" ");

        if (wor.length < 2) {
            return "There is not enough words!";
        }
        String[] words = new String[wor.length];
        int j = 0;
        for (int i = wor.length-1; i >=0; i--, j++) {
            words[j] = wor[i];
        }

        StringBuilder sb = new StringBuilder();
        for (String word : words) {
            sb.append(Character.toUpperCase(word.charAt(0)));
            sb.append(word.substring(1).toLowerCase());
            sb.append(" ");
        }

        return sb.toString().trim();
    }

    //      System.out.println("\n--------------------Task6-----------------------");

    public static String removeStringSpecialsDigits(String str) {
        return str.replaceAll("[^A-Za-z ]", "");
    }

    //      System.out.println("\n--------------------Task7-----------------------");

    public static String[] removeArraySpecialsDigits(String[] array) {
        for (int i = 0; i < array.length; i++) {
            array[i] = array[i].replaceAll("[^A-Za-z ]", "");
        }return array;
    }

    //      System.out.println("\n--------------------Task8-----------------------");

    public static ArrayList<String> removeAndReturnCommons(ArrayList<String> list1, ArrayList<String> list2) {
        ArrayList<String> commons = new ArrayList<>();

        for (int i = 0; i < list1.size(); i++) {
            if (list2.contains(list1.get(i))) {
                commons.add(list1.get(i));
                list1.remove(i);
                list2.remove(list1.get(i));
                i--;}
        }return commons;
    }

    //      System.out.println("\n--------------------Task9-----------------------");

    public static ArrayList<String> noXInVariables(ArrayList<String> list) {
        ArrayList<String> noXList = new ArrayList<>();
        for (String element : list) {
            if (!element.equalsIgnoreCase("x")) {
                noXList.add(element.replaceAll("[xX]", ""));
            }
        }
        return noXList;
    }

    //      System.out.println("\n--------------------EndOfTasks-----------------------");











    public static void main(String[] args) {

        System.out.println("\n--------------------Task1-----------------------");

        System.out.println(Homework13.hasLowerCase(""));
        System.out.println(Homework13.hasLowerCase("JAVA"));
        System.out.println(Homework13.hasLowerCase("125$"));
        System.out.println(Homework13.hasLowerCase("hello"));

        System.out.println("\n--------------------Task2-----------------------");

        System.out.println(Homework13.noZero(new ArrayList<Integer>(Arrays.asList(1))));
        System.out.println(Homework13.noZero(new ArrayList<Integer>(Arrays.asList(1,1,10))));
        System.out.println(Homework13.noZero(new ArrayList<Integer>(Arrays.asList(0,1,10))));
        System.out.println(Homework13.noZero(new ArrayList<Integer>(Arrays.asList(0,0,0))));

        System.out.println("\n--------------------Task3-----------------------");

        System.out.println(Arrays.deepToString(Homework13.numberAndSquare(new int[] {1,2,3})));
        System.out.println(Arrays.deepToString(Homework13.numberAndSquare(new int[] {0,3,6})));
        System.out.println(Arrays.deepToString(Homework13.numberAndSquare(new int[] {1,4})));

        System.out.println("\n--------------------Task4-----------------------");

        System.out.println(Homework13.containsValue(new String[]{"abc", "foo", "java"}, "hello"));
        System.out.println(Homework13.containsValue(new String[]{"abc", "def", "123"}, "Abc"));
        System.out.println(Homework13.containsValue(new String[]{"abc", "def", "123", "Java", "Hello"}, "123"));

        System.out.println("\n--------------------Task5-----------------------");

        System.out.println(Homework13.reverseSentence("Hello"));
        System.out.println(Homework13.reverseSentence("Java is fun"));
        System.out.println(Homework13.reverseSentence("This is a sentence"));

        System.out.println("\n--------------------Task6-----------------------");

        System.out.println(Homework13.removeStringSpecialsDigits("123Java #$%is fun"));
        System.out.println(Homework13.removeStringSpecialsDigits("Selenium"));
        System.out.println(Homework13.removeStringSpecialsDigits("Selenium 123#$%Cypress"));

        System.out.println("\n--------------------Task7-----------------------");

        System.out.println(Arrays.toString(Homework13.removeArraySpecialsDigits(new String[]{"123Java", "#$%is", "fun"})));
        System.out.println(Arrays.toString(Homework13.removeArraySpecialsDigits(new String[]{"Selenium", "123$%", "###"})));
        System.out.println(Arrays.toString(Homework13.removeArraySpecialsDigits(new String[]{"Selenium", "123#$%Cypress"})));

        System.out.println("\n--------------------Task8-----------------------");

        System.out.println(Homework13.removeAndReturnCommons(new ArrayList<>(Arrays.asList("Java", "is", "fun")), new ArrayList<>(Arrays.asList("abc", "xyz", "123"))));
        System.out.println(Homework13.removeAndReturnCommons(new ArrayList<>(Arrays.asList("Java", "is", "fun")), new ArrayList<>(Arrays.asList("Java", "C#", "Python"))));
        System.out.println(Homework13.removeAndReturnCommons(new ArrayList<>(Arrays.asList("Java", "C#", "C#")), new ArrayList<>(Arrays.asList("Python", "C#", "C++"))));

        System.out.println("\n--------------------Task9-----------------------");

    System.out.println(Homework13.noXInVariables(new ArrayList<>(Arrays.asList("abc", "123", "#$%"))));
    System.out.println(Homework13.noXInVariables(new ArrayList<>(Arrays.asList("xyz", "123", "#$%]"))));
    System.out.println(Homework13.noXInVariables(new ArrayList<>(Arrays.asList("x", "123", "#$%"))));
    System.out.println(Homework13.noXInVariables(new ArrayList<>(Arrays.asList("xyXyxy", "Xx", "ABC"))));

        System.out.println("\n--------------------EndOfTasks-----------------------");

    }
}
