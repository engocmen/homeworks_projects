package homeworks;
import com.sun.xml.internal.ws.api.model.wsdl.WSDLOutput;
import utilities.ScannerHelper;

import java.util.Arrays;
import java.util.Scanner;
public class Homework06 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("\n--------------------Task1-----------------------");

        int[] numbers = new int[10];
        numbers[2] = 23;
        numbers[4] = 12;
        numbers[7] = 34;
        numbers[9] = 7;
        numbers[6] = 15;
        numbers[0] = 89;
        System.out.println(numbers[3]);
        System.out.println(numbers[0]);
        System.out.println(numbers[9]);
        System.out.println(Arrays.toString(numbers));

        System.out.println("\n--------------------Task2-----------------------");

        String[] words = new String[5];
        words[1] = "abc";
        words[4] = "xyz";
        System.out.println(words[3]);
        System.out.println(words[0]);
        System.out.println(words[4]);
        System.out.println(Arrays.toString(words));

        System.out.println("\n--------------------Task3-----------------------");

        int[] arr ={23, -34, -56, 0, 89, 100};
        System.out.println(Arrays.toString(arr));
        Arrays.sort(arr);
        System.out.println(Arrays.toString(arr));

        System.out.println("\n--------------------Task4-----------------------");

        String[] countries ={"Germany", "Argentina", "Ukraine", "Romania"};
        System.out.println(Arrays.toString(countries));
        Arrays.sort(arr);
        System.out.println(Arrays.toString(countries));

        System.out.println("\n--------------------Task5-----------------------");

        String[] dogs = {"Scooby Doo", "Snoopy", "Blue", "Pluto", "Dino", "Sparky"};
        System.out.println(Arrays.toString(dogs));
        boolean answer = false;
        for (int i = 0; i < dogs.length; i++) {
        if (dogs[i].equals("Pluto")){ answer = true;}}
            System.out.println(answer);

        System.out.println("\n--------------------Task6-----------------------");

        String[] cats ={"Garfield", "Tom", "Sylvester", "Azrael"};
        Arrays.sort(cats);
        System.out.println(Arrays.toString(cats));
        System.out.println(Arrays.toString(cats).contains("Garfield")&&Arrays.toString(cats).contains("Felix"));

        System.out.println("\n--------------------Task7-----------------------");

        double[] numbers1 = {10.5, 20.75, 70, 80, 15.75};
        System.out.println(Arrays.toString(numbers1));
        for (double element : numbers1){
            System.out.println(element);}

        System.out.println("\n--------------------Task8-----------------------");

        char[] characters = {'A', 'b', 'G', 'H', '7', '5', '&', '*', 'e', '@', '4'};
        System.out.println(Arrays.toString(characters));
        String chars = Arrays.toString(characters);
        int letters = 0, upperCase = 0, lowerCase = 0, digit = 0, special = 0;
        for (int i = 0; i < characters.length; i++) {
        if (Character.isLetter(characters[i])) letters++;
        if (Character.isUpperCase(characters[i])) upperCase++;
        if (Character.isLowerCase(characters[i])) lowerCase++;
        if (Character.isDigit(characters[i])) digit++;
        if (!Character.isLetterOrDigit(characters[i])) special++;
        }
        System.out.println("Letters = " + letters + "\n" +
                "Uppercase letters = " + upperCase +"\n" +
                "Lowercase letters = " + lowerCase + "\n" +
                "Digits = " + digit + "\n" +
                "Special characters = " + special );

        System.out.println("\n--------------------Task9-----------------------");

        String[] school = {"Pen", "notebook", "Book", "paper", "bag", "pencil", "Ruler"};
        System.out.println(Arrays.toString(school));

        int upper = 0, lower = 0, borP = 0, bookOrPen = 0;

        for (int i = 0; i < school.length; i++) {
            if (Character.isUpperCase(school[i].charAt(0))) upper++;
            if (Character.isLowerCase(school[i].charAt(0))) lower++;
            if (school[i].toLowerCase().charAt(0)=='b' || school[i].toLowerCase().charAt(0)=='p' ) borP++;
            if (school[i].toLowerCase().contains("book")||school[i].toLowerCase().contains("pen")) bookOrPen++;}

        System.out.println("Elements starts with uppercase = " + upper + "\n" +
                "Elements starts with lowercase = " + lower + "\n" +
                "Elements starting with B or P = " + borP + "\n" +
                "Elements having book or pen = " + bookOrPen);

        System.out.println("\n--------------------Task10-----------------------");

        int[] numbers2 = {3, 5, 7, 10, 0, 20, 17, 10, 23, 56, 78};
        System.out.println(Arrays.toString(numbers2));
        Arrays.sort(numbers2);
        int positive = 0, negative = 0, ten = 0;
        for (int i = 0; i < numbers2.length; i++) {
            if(numbers2[i]>10) positive++;
            if(numbers2[i]<10) negative++;
            if(numbers2[i]==10) ten++;}

        System.out.println("Elements that are more than 10 = " + positive + "\n" +
                "Elements that are less than 10 = " + negative + "\n" +
                "Elements that are 10 = " + ten);

        System.out.println("\n--------------------Task11-----------------------");

        int[] first = {5, 8, 13, 1, 2};
        int[] second = {9, 3, 67, 1, 0};
        int[] third = new int[5];
        System.out.println("1st array is = " + Arrays.toString(first));
        System.out.println("2nd array is = " +  Arrays.toString(second));
        for (int i = 0; i < first.length; i++) {
            if(first[i]>second[i]) third[i] = first[i];
            else third[i] = second[i];}

        System.out.println("3rd array is = " + Arrays.toString(third));


        System.out.println("\n--------------------EndofTasks-----------------------");
    }
}
