package homeworks;

import java.util.Scanner;

public class Homework03 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("\n--------------------Task1------------------------");


        System.out.println("User please enter Two number.");
        int numb1 = input.nextInt(), numb2 = input.nextInt();


        System.out.println("The difference between numbers is =  " + Math.abs(numb1 - numb2));

        System.out.println("\n--------------------Task2------------------------");


        System.out.println("User please enter Five numbers.");
        int num1 = input.nextInt(), num2 = input.nextInt(), num3 = input.nextInt(), num4 = input.nextInt(),
                num5 = input.nextInt();
        int a = Math.max(num1 , num2);
        int b = Math.max(num3 , num4);
        int c = Math.min(num1 , num2);
        int d = Math.min(num3 , num4);


        System.out.println("Max value = " + (Math.max(Math.max(a, b), num5)));
        System.out.println("Min value = " + (Math.min(Math.min(c, d), num5)));

        System.out.println("\n--------------------Task3------------------------");

        int randomNum1 = (int) (Math.random() * 51) + 50, randomNum2 = (int) (Math.random() * 51) + 50;
        int randomNum3 = (int) (Math.random() * 51) + 50;
        System.out.println("Number 1 = " + randomNum1);
        System.out.println("Number 2 = " + randomNum2);
        System.out.println("Number 3 = " + randomNum3);
        System.out.println("The sum of numbers is = " + (randomNum1 + randomNum2 + randomNum3));

        System.out.println("\n--------------------Task4------------------------");

        double monAlex = 125, monMike = 220, payment = 25.5;
        monAlex -= payment;
        monMike += payment;
        System.out.println("Alex's money: $" + monAlex);
        System.out.println("Mike's money: " + monMike);

        System.out.println("\n--------------------Task5------------------------");

        double bicycle = 390, moneyDavid = 15.6;
        int days = (int) (bicycle / moneyDavid);
        System.out.println(days);

        System.out.println("\n--------------------Task6------------------------");

        String s1 = "5", s2 = "10";
        int i1 = Integer.parseInt(s1), i2 = Integer.parseInt(s2);

        System.out.println("-Sum of "+ s1 + " and " + s2 + " is = " + (i1 + i2) +"\n"+
        "-Product of "+ s1 + " and " + s2 + " is = " + i1 * i2 +"\n"+
        "-Division of "+ s1 + " and " + s2 + " is = " + i1 / i2 +"\n"+
        "-Subtraction of "+ s1 + " and " + s2 + " is = " + (i1 - i2) +"\n"+
        "-Remainder of "+ s1 + " and " + s2 + " is = " + i1 % i2);

        System.out.println("\n--------------------Task7------------------------");

        String st1 = "200", st2 = "-50";
        int t1 = Integer.parseInt(st1), t2 = Integer.parseInt(st2);

        System.out.println("The greatest value is = " + Integer.max(t1, t2) + "\n" +
                "The smallest value is = " + Integer.min(t1, t2) + "\n" +
                "The average is = " + (t1 + t2) / 2 + "\n" +
                "The absolute difference is = " + Math.abs(t1 - t2));

        System.out.println("\n--------------------Task8------------------------");

        double cBank = .96;
        double day1 = 24 / cBank;
        double day2 = 168 / cBank;
        double month = 150 * cBank;
        Integer days1 = (int) day1, days2 = (int) day2;
        System.out.println(days1 + " days");
        System.out.println(days2 + " days");
        System.out.println("$" + month);

        System.out.println("\n--------------------Task9------------------------");

        double jessDaily = 62.5, newComp = 1250;
        Integer bought = (int) (newComp / jessDaily);
        System.out.println(bought);

        System.out.println("\n--------------------Task10------------------------");

        double danCar = 14265;
        double option1 = danCar / 475.5, option2 = danCar / 951;
        System.out.println("Option 1 will take " + (int) (option1) + " months");
        System.out.println("Option 2 will take " + (int) (option2) + " months");

        System.out.println("\n--------------------Task11------------------------");

        System.out.println("User please enter two numbers");
        int number1 = input.nextInt(), number2 = input.nextInt();
        double number3 = (double) number1 / number2;
        System.out.println(number3);

        System.out.println("\n--------------------Task12------------------------");

        int randomNumb1 = (int) (Math.random() * 101) + 0, randomNumb2 = (int) (Math.random() * 101) + 0;
        int randomNumb3 = (int) (Math.random() * 101) + 0;
        System.out.println(randomNumb1);
        System.out.println(randomNumb2);
        System.out.println(randomNumb3);
        if(randomNumb1 > 25 && randomNumb2 > 25 && randomNumb3 > 25) System.out.println("true");
        else System.out.println("false");

        System.out.println("\n--------------------Task13------------------------");

        System.out.println("User please enter a number 1 - 7!");
        int dayNum = input.nextInt();
        if(dayNum == 1) System.out.println("The number entered returns Monday");
        else if (dayNum == 2) System.out.println("The number entered returns Tuesday");
        else if (dayNum == 3) System.out.println("The number entered returns Wednesday");
        else if (dayNum == 4) System.out.println("The number entered returns Thursday");
        else if (dayNum == 5) System.out.println("The number entered returns Friday");
        else if (dayNum == 6) System.out.println("The number entered returns Saturday");
        else if (dayNum == 7) System.out.println("The number entered returns Sunday");
        else System.out.println("You have entered an incorrect response, please try again");

        System.out.println("\n--------------------Task14------------------------");

        System.out.println("Tell me your exam results?");
        int exam1 = input.nextInt(), exam2 = input.nextInt(), exam3 = input.nextInt();
        int passFail = (exam1 + exam2 + exam3) / 3;
        if (passFail >= 70) System.out.println("YOU PASSED!");
        else System.out.println("YOU FAILED!");

        System.out.println("\n--------------------Task15------------------------");

        System.out.println("Enter 3 Numbers");
        int nu1 = input.nextInt(), nu2 = input.nextInt(), nu3 = input.nextInt();
        if (nu1 == nu2 & nu2 == nu3) System.out.println("TRIPLE MATCH");
        else if (nu1 == nu2 || nu2 == nu3 || nu1 == nu3) System.out.println("DOUBLE MATCH");
        else System.out.println("NO MATCH");

        System.out.println("\n--------------------EndofTask------------------------");


    }
}
