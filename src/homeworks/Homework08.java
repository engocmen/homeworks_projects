package homeworks;

import java.util.Arrays;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Homework08 {

    //      System.out.println("\n--------------------Task1-----------------------");

    public static int countConsonants(String vowels){
    int count = 0;
        for (int i = 0; i < vowels.length(); i++) {
            if (Pattern.matches("[\\w^aeiouAEIOU]", "" + vowels.charAt(i))) count++;}
        return count;}

    //      System.out.println("\n--------------------Task2-----------------------");

public static String[] wordArray(String str){
        return str.split("[\\W]+");
}

    //      System.out.println("\n--------------------Task3-----------------------");

public static String removeExtraSpaces(String str){
        return str.replaceAll("[\\s]+", " ");}

    //      System.out.println("\n--------------------Task4-----------------------");


    public static int count3OrLess(String words){
        int count = 0;
        Scanner scan = new Scanner(System.in);
        System.out.println("Please enter a sentence.");
        words = scan.nextLine();
        String[] str = words.split("[\\s]+");
        for (String s : str) {
            if(s.length()<4) count++;
        }return count;}

    //      System.out.println("\n--------------------Task5-----------------------");

    public static boolean isDateFormatValid(String str){
        return str.matches("[\\d]{2}/[\\d]{2}/[\\d]{4}");
    }

    //      System.out.println("\n--------------------Task6-----------------------");

    public static boolean isEmailFormatValid(String str){
        return str.matches("[\\w.]{2,}@[\\w.]{2,}\\.[\\w.]{2,}");
    }

    //      System.out.println("\n--------------------EndOfTask-----------------------");



}
