package homeworks;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Homework10 {

    //      System.out.println("\n--------------------Task1-----------------------");

    public static int countWords(String str){
        return (int)Arrays.stream(str.trim().split("[\\s]+")).count();
    }

    //      System.out.println("\n--------------------Task2-----------------------");

    public static int countA(String str){
        int count = 0;
        for (int i = 0; i < str.length() ; i++) {
            if (str.toLowerCase().charAt(i)=='a') count++;}
        return count;}

    //      System.out.println("\n--------------------Task3-----------------------");

    public static int countPos(int[] nums){
        int count = 0;
        for (int num : nums) {
            if (num > 0) count++;}
    return count;}

    //      System.out.println("\n--------------------Task4-----------------------");

public static ArrayList<Integer> removeDuplicateNumbers(ArrayList<Integer> nums){
    nums.removeIf(num -> num < 1);
return nums;}

    //      System.out.println("\n--------------------Task5-----------------------");

    public static ArrayList<String> removeDuplicateElements(ArrayList<String> words){
        HashSet<String> hashSet = new HashSet<>(words);
        ArrayList<String> str = new ArrayList<>(hashSet);
        return str;}

    //      System.out.println("\n--------------------Task6-----------------------");

public static String removeExtraSpaces(String str){
return str.trim().replaceAll("[\\s]+", " ");}

    //      System.out.println("\n--------------------Task7-----------------------");

    public static int[] add2arrays(int[] arr1, int[] arr2){
        int[] arr3;
        if (arr1.length>arr2.length)
            arr3 = arr1;
        else arr3 = arr2;
        for (int i = 0; i < Math.min(arr1.length, arr2.length); i++) {
            arr3[i] = arr1[i] + arr2[i];}
    return arr3;}

    //      System.out.println("\n--------------------Task8-----------------------");

    public static int findClosestTo10(int[] nums){
        int counter = Integer.MAX_VALUE;
        for (int num : nums) {
            if (Math.abs(10 - num) <= Math.abs(counter-10) && num!=10)
                if (Math.abs(10-num)==Math.abs(counter-10)) counter =Math.min(num, counter);
            else counter = num;
        }return counter;}








    public static void main(String[] args) {
        int[] arr1 = {3, 0, 0, 7, 5, 10};
        int[] arr2 = {6, 3, 2};
        int[] nums = {-23, -4, 0, 2, 5, 90, 123};
        int[] numbers = {10, -13, 5, 70, 15, 57};

        ArrayList<Integer> numbs = new ArrayList<>(Arrays.asList(-23, -4, 0, 2, 5, 90, 123));
        ArrayList<String> words = new ArrayList<>(Arrays.asList("java", "C#", "ruby", "JAVA", "ruby", "C#", "C++"));
        String str = "   JAva  and also  is     fun     ";
        System.out.println(Homework10.findClosestTo10(numbers));
    }

}
