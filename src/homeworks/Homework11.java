package homeworks;

import sun.util.calendar.BaseCalendar;

import java.awt.*;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

public class Homework11 {

    //      System.out.println("\n--------------------Task1-----------------------");

    public static String noSpace(String words){
return words.replaceAll("[\\s]+", "");
    }

    //      System.out.println("\n--------------------Task2-----------------------");

    public static String replaceFirstLast(String str) {
        str=str.trim();
        if(str.length()<2)return "";
    return ""+str.charAt(str.length()-1)+str.substring(1, str.length()-1)+str.charAt(0);
    }
    //      System.out.println("\n--------------------Task3-----------------------");

    public static boolean hasVowel(String str){
return (str.matches(".[aeiouAEIOU]+"));}

    //      System.out.println("\n--------------------Task4-----------------------");
    public static String checkAge(int age) {
        int diff = (2022 - age);
        if (diff > 100 || diff < 0) return "AGE IS NOT VALID";
        else if (diff > 16) return "AGE IS ALLOWED";
        else return "AGE IS NOT ALLOWED";}

    //      System.out.println("\n--------------------Task5-----------------------");

    public static int averageOfEdges(int[] nums){
        Arrays.sort(nums);
        int min = Math.min(nums[0], nums[1]);
        int max = Math.max(nums[nums.length-1], nums[nums.length-2]);
        return (max+min)/2;
    }
    //      System.out.println("\n--------------------Task6-----------------------");

    public static String[] noA(String[] str){
        for (int i = 0; i < str.length; i++) {
            if(str[i].toLowerCase().startsWith("a")) str[i] = "###";
        }
    return str;}

    //      System.out.println("\n--------------------Task7-----------------------");

    public static int[] no3or5(int[] nums){
        for (int i = 0; i < nums.length; i++) {
            if (nums[i]%15==0) nums[i] = 101;
            else if (nums[i]%5==0) nums[i] = 99;
            else if (nums[i]%3==0) nums[i] = 100;
        }return nums;}

    //      System.out.println("\n--------------------Task8-----------------------");

    public static int countPrimes(int[] nums){
        int count = 0;
        for (int num : nums) {
            for (int i = 2; i < num; ++i) {
                if (num % i == 0) {
                    count++;
                    break;
                }}} return nums.length-count;}

    //      System.out.println("\n--------------------EndOfTask-----------------------");












    public static void main(String[] args) {


        String words = "sa";
        int[] nums = {-10, -3, 0, 1};
        String[] str = {"appium", "a23", "ABC", "java"};
        System.out.println(Homework11.countPrimes(nums));

    }
}
