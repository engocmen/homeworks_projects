package homeworks;

public class Homework01 {
    public static void main(String[] args) {
        /*
        TASK1
        JAVA ->
J	74		1	0	0	1	0	1	0
A	65		1	0	0	0	0	0	1
V	86		1	0	1	0	1	1	0
A	65		1	0	0	0	0	0	1

        SELENIUM ->
S	83	    1	0	1	0	0	1	1
E	69	    1	0	0	0	1	0	1
L	76  	1	0	0	1	1	0	0
E	69	    1	0	0	0	1	0	1
N	78  	1	0	0	1	1	1	0
I	73	    1	0	0	1	0	0	1
U	85  	1	0	1	0	1	0	1
M	77  	1	0	0	1	1	0	1

         */


        /*
        TASK2
        The letters are =
Decimal - ASCII -  Letters
1001101	    77	    M
1101000 	104 	h
1111001	    121 	y
1010011	    83	    S
1101100 	108	    l

         */


        /*
        TASK3
        */

        System.out.println("I start to practice \"JAVA\" today, and I like it.");
        System.out.println("The secret of getting ahead is getting started.");
        System.out.println("\"Don't limit yourself. \"");
        System.out.println("Invest in your dreams. Grind now. Shine later.");
        System.out.println("It’s not the load that breaks you down, it’s the way you carry it.");
        System.out.println("The hard days are what make you stronger.");
        System.out.println("You can waste your lives drawing lines. Or you can live your life crossing them.\n");


        /*
        TASK4
         */
        System.out.println("\tJava is easy to write and easy to run—this is the foundational" +
                "\nstrength of Java and why many developers program in it. When you" +
                "\nwrite Java once, you can run it almost anywhere at any time.\n" +
                "\n" +
                "\tJava can be used to create complete applications that can run on" +
                "\na single computer or be distributed across servers and clients in a" +
                "\nnetwork.\n" +
                "\n" +
                "\tAs a result, you can use it to easily build mobile applications or" +
                "\nrun-on desktop applications that use different operating systems and" +
                "\nservers, such as Linux or Windows.\n");

         /*
        TASK5
         */
        byte myAge = 38;
        int myFavoriteNumber = 5651;
        String myHeight = "6'1";
        short myWeight = 190;
        char myFavoriteCharacter = '$';

        System.out.println(myAge);
        System.out.println(myFavoriteNumber);
        System.out.println(myHeight);
        System.out.println(myWeight);
        System.out.println(myFavoriteCharacter);
    }
}