package projects;

public class Project03 {
    public static void main(String[] args) {

        System.out.println("\n--------------------Task1------------------------");

        String s1 = "24", s2 = "5";
        int i1 = Integer.parseInt(s1), i2 = Integer.parseInt(s2);
        System.out.println("The sum of " + i1 + " and " + i2 + " = " + (i2 + i1) + "\n" + "The subtraction of " + i1 +
                " and " + i2 + " = " + (i2 - i1) + "\n" + "The division of " + i1 + " and " + i2 + " = " + i2 / i1 +
                "\n" + "The multiplication of " + i1 + " and " + i2 + " = " + i2 * i1 + "\n" + "The remainder of " +
                i1 + " and " + i2 + " = " + i2 % i1);
        System.out.println("\n--------------------Task2------------------------");
        int ranNum1to35 = (int) (Math.random() * (35) + 1);

        if (ranNum1to35 == 2 || ranNum1to35 == 3 || ranNum1to35 == 5 || ranNum1to35 == 7 || ranNum1to35 == 11 ||
                ranNum1to35 == 13 || ranNum1to35 == 17 || ranNum1to35 == 19 || ranNum1to35 == 23 || ranNum1to35 == 29 ||
                ranNum1to35 == 31)
        System.out.println(ranNum1to35 + " is a prime number");
        else
            System.out.println(ranNum1to35 + " is not a prime number");

        System.out.println("\n--------------------Task3------------------------");
        int num1 = (int) (Math.random() * (50) + 1);
        int num2 = (int) (Math.random() * (50) + 1);
        int num3 = (int) (Math.random() * (50) + 1);

        System.out.println("Lowest number is = " + Math.min(Math.min(num1, num2), num3));
        if(num1 > num2 && num2 > num3) {
            System.out.println("Middle number is = " + num2);
        }else if (num2 > num3 && num3 > num1) {
            System.out.println("Middle number is = " + num3);
        }else
            System.out.println("Middle number is = " + num1);
        System.out.println("Greatest number is = " + Math.max(Math.max(num1, num2), num3));

        System.out.println("\n--------------------Task4------------------------");

char c = 5;
int numAscii = c;
if(numAscii > 96 && numAscii > 123) System.out.println("The letter is lowercase.");
else if (numAscii > 64 && numAscii < 91) System.out.println("The letter is lowercase.");
else System.out.println("Invalid Character");

        System.out.println("\n--------------------Task5------------------------");

        char C = 5;
        int numAscii1 = C;
        if(numAscii1 < 65 || (numAscii1 > 90 && numAscii1 < 97) || numAscii1 > 122) System.out.println("Invalid character detected!!!");
        else if (numAscii1 == 65 || numAscii1 == 69 || numAscii1 == 73 || numAscii1 == 79 || numAscii1 == 85 ||
                numAscii1 == 97 || numAscii1 == 101 || numAscii1 == 105 || numAscii1 == 111 || numAscii1 == 117)
            System.out.println("The letter is vowel");
        else System.out.println("The letter is consonant");

        System.out.println("\n--------------------Task6------------------------");

        char c1 = 5;
        int numAscii2 = c1;
        if((numAscii2 > 64 && numAscii2 > 91) || (numAscii2 > 96 && numAscii2 < 123) || (numAscii2 > 47 && numAscii2 < 58))
            System.out.println("Invalid character detected!!!");
        else System.out.println("Special character is = " + c1);

        System.out.println("\n--------------------Task7------------------------");

        char c2 = 5;
        int numAscii3 = c2;
        if((numAscii2 > 64 && numAscii2 > 91) || (numAscii2 > 96 && numAscii2 < 123))
            System.out.println("Character is a letter");
        else if(numAscii2 > 47 && numAscii2 < 58)
            System.out.println("Character is a digit");
        else
            System.out.println("Character is a special character");


    }
}
