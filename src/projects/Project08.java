package projects;

import java.util.Arrays;
import java.util.HashSet;

public class Project08 {


    //      System.out.println("\n--------------------Task1-----------------------");

public static String findClosestDistance(int[] arr){
    int first=0, second=0, total=Integer.MAX_VALUE;
if(arr.length<2)return "-1\n-The reason the result is -1 for above example is because the array length is less than 2 " +
        "and we return -1 in this case.";
    for (int i = 0; i < arr.length; i++) {
        for (int j = i+1; j < arr.length; j++) {
        if((Math.abs(arr[i]-arr[j]))<total){
            total = Math.abs(arr[i]-arr[j]);
        first = arr[i];
        second = arr[j];}}}
    return "" + total + "\n-The reason the result is " + total + " for above example is because " + first +" and " + second +
            " are closest elements in the array and the difference between them is " + total + ".";
}

    //      System.out.println("\n--------------------Task2-----------------------");

    public static int findSingleNumber(int[] nums) {
        String answer = Arrays.toString(nums);
        for (int num : nums) {
            if (answer.indexOf(""+num) == answer.lastIndexOf(""+num)) return num;
        }return 0;}

    //      System.out.println("\n--------------------Task3-----------------------");

    public static char findFirstUniqueCharacter(String words) {
char[] arr = words.toCharArray();
        for (char c : arr) {
            if (words.indexOf(""+c) == words.lastIndexOf(""+c)) return c;
        }return ' ';}

    //      System.out.println("\n--------------------Task4-----------------------");


    public static int findMissingNumber(int[] nums){
    Arrays.sort(nums);
        for (int i = 0; i < nums.length; i++) {
            if (nums[i]+1!=nums[i+1]) return nums[i]+1;
        }
    return -1;}

    //      System.out.println("\n--------------------EndOfTask-----------------------");

    public static void main(String[] args) {

int[] nums = {5, 3, -1, 3, 5, 7, -1, 7, 2};
        System.out.println(Project08.findSingleNumber(nums));

    }

}
