package projects;

import java.util.Scanner;

public class Project02 {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);

        System.out.println("\n--------------------Task1------------------------");


        System.out.println("Please enter 3 numbers");
        int numb1 = input.nextInt(), numb2 = input.nextInt(), numb3 = input.nextInt();
        input.nextLine();

        System.out.println("The product of the numbers entered is = " + (numb1 * numb2 * numb3));

        System.out.println("\n--------------------Task2------------------------");


        System.out.println("What is your first name?");
        String fName = input.nextLine();
        System.out.println("What is your last name?");
        String lName = input.nextLine();
        System.out.println("What is your year of birth?");
        int yOB = input.nextInt();
        input.nextLine();

        System.out.println(fName + " " + lName + "'s age is = " + (2022 - yOB));

        System.out.println("\n--------------------Task3------------------------");


        System.out.println("What is your full name?");
        String fullName = input.nextLine();
        System.out.println("What is your weight?");
        double weight = input.nextDouble();
        input.nextLine();

        System.out.println(fullName + "'s weight is = " + (weight * 2.205) + " lbs.");

        System.out.println("\n--------------------Task4------------------------");


        System.out.println("What is your full name?");
        String fullName1 = input.nextLine();
        System.out.println("What is your age?");
        int age1 = input.nextInt();
        input.nextLine();
        System.out.println("What is your full name?");
        String fullName2 = input.nextLine();
        System.out.println("What is your age?");
        int age2 = input.nextInt();
        input.nextLine();
        System.out.println("What is your full name?");
        String fullName3 = input.nextLine();
        System.out.println("What is your age?");
        int age3 = input.nextInt();
        input.nextLine();

        System.out.println(fullName1 + "'s age is = " + age1 +".");
        System.out.println(fullName2 + "'s age is = " + age2 +".");
        System.out.println(fullName3 + "'s age is = " + age3 +".");
        System.out.println("The average age is " + (age1 + age2 + age3) / 3 + ".");
        System.out.println("The eldest age is " + Math.max(Math.max(age1, age2), age3) + ".");
        System.out.println("The youngest age is " + Math.min(Math.min(age1, age2), age3) + ".");

    }
}
