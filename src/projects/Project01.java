package projects;

public class Project01 {
    public static void main(String[] args) {

        System.out.println("--------------------Task1------------------------");

        String name = "Engin";
        System.out.println("My name is " + name + ".");

        System.out.println("\n--------------------Task2------------------------");

    char nameCharacter1 = 'E';
    char nameCharacter2 = 'n';
    char nameCharacter3 = 'g';
    char nameCharacter4 = 'i';
    char nameCharacter5 = 'n';

        System.out.println("Name letter 1 is = " + nameCharacter1);
        System.out.println("Name letter 2 is = " + nameCharacter2);
        System.out.println("Name letter 3 is = " + nameCharacter3);
        System.out.println("Name letter 4 is = " + nameCharacter4);
        System.out.println("Name letter 5 is = " + nameCharacter5);

        System.out.println("\n--------------------Task3------------------------");


        String myFavMovie ="Matrix";
        String myFavSong ="Slow Motion";
        String myFavCity ="Chicago";
        String myFavActivity ="Sky Diving";
        String myFavSnack ="Snickers";

        System.out.println("My favorite movie is = " + myFavMovie);
        System.out.println("My favorite Song is = " + myFavSong);
        System.out.println("My favorite City is = " + myFavCity);
        System.out.println("My favorite Activity is = " + myFavActivity);
        System.out.println("My favorite Snack is = " + myFavSnack);

        System.out.println("\n--------------------Task4------------------------");


        int myFavNumber, numberOfStatesIVisited, numberOfCountriesIVisited;
        myFavNumber = 7;
        numberOfStatesIVisited = 4;
        numberOfCountriesIVisited = 5;

        System.out.println("My favorite number is = " + myFavNumber);
        System.out.println("The number of States I have Visited = " + numberOfStatesIVisited);
        System.out.println("The number of Countries I have Visited = " + numberOfCountriesIVisited);

        System.out.println("\n--------------------Task5------------------------");


        boolean amIAtSchoolToday = true;

        System.out.println("I am at school today = " + amIAtSchoolToday);

        System.out.println("\n--------------------Task6------------------------");


        boolean isWeatherNiceToday = true;

        System.out.println("Weather is nice today = " + isWeatherNiceToday);

    }
}
