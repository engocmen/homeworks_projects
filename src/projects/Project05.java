package projects;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Project05 {
    public static void main(String[] args) {

        System.out.println("\n--------------------Task1-----------------------");

        int[] nums = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        Arrays.sort(nums);
        System.out.println("Smallest = " + nums[0]);
        System.out.println("Greatest = " + nums[nums.length-1]);

        System.out.println("\n--------------------Task2-----------------------");

        int max = Integer.MIN_VALUE; // -1231232324
        int min = Integer.MAX_VALUE;

        for (int n : nums) {
            if (max < n){
                max = n;
            }

            if (min > n){
                min = n;
            }
        }

        if (nums.length > 0){
            System.out.println("Smallest is = " + max);
            System.out.println("Greatest is = " + min);
        }else{
            System.out.println("Array is empty!");
        }

        System.out.println("\n--------------------Task3-----------------------");

        Arrays.sort(nums);
        System.out.println("Second Smallest = " + nums[1]);
        System.out.println("Second Greatest = " + nums[nums.length-2]);

        System.out.println("\n--------------------Task4-----------------------");


            int secondMax = Integer.MIN_VALUE, secondMin = Integer.MAX_VALUE;
        max = Integer.MIN_VALUE;

        for (int n : nums) {
            if (n > max) max = n;
        }

        min = Integer.MAX_VALUE;
        for (int n : nums) {
            if (n < min) min = n;
        }

            for (int n : nums) {
                if (n > secondMax && n < max){
                    secondMax = n;
                }

                if (n < secondMin && n > min){
                    secondMin = n;
                }}
        System.out.println("Second Smallest = " + secondMin );
            System.out.println("Second Greatest = " + secondMax );

        System.out.println("\n--------------------Task5-----------------------");

        String[] numDup = {"foo", "bar", "Foo", "bar", "6", "abc", "6", "xyz"};
        String dup = "";

        for (int i = 0; i < numDup.length - 1; i++) {
            for (int j = i + 1; j < numDup.length; j++) {
                if (dup.contains(numDup[i] + "")) break;


                if (numDup[i] == numDup[j]){
                    dup += numDup[j] + ","; }}}

        System.out.println(dup.replaceAll(",", "\n"));

        System.out.println("--------------------Task6-----------------------");

        ArrayList<String> school = new ArrayList<>(Arrays.asList("pen", "eraser", "pencil", "pen", "123", "abc", "pen", "eraser"));
        Collections.sort(school);
        ArrayList<String> copies = new ArrayList<>();
        String copy = "";

            for (int i = 0; i < school.size()-1; i++) {
                if(school.get(i)==school.get(i+1)) copies.add(school.get(i));}

        for (int i = 0; i < copies.size()-1; i++) {
            if (copies.get(i).equals(copies.get(i+1))) copy += copies.get(i);}

        System.out.println(copy);

        System.out.println("\n--------------------EndOfTasks-----------------------");



    }
}
