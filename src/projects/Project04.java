package projects;
import utilities.MathHelper;
import utilities.RandomNumberGenerator;
import utilities.ScannerHelper;

import java.util.Scanner;
public class Project04 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("\n--------------------Task1------------------------");

        String str = ScannerHelper.getAString();
        String result = "";
        for (int i = 0; i < str.length()-1; i++) {
            if(str.length()<8) result = "This String does not have 8 characters";
            else result = str.substring(str.length()-4)+str.substring(4, str.length()-4)+str.substring(0,4);
        }
        System.out.println(result);

        System.out.println("\n--------------------Task2------------------------");

        System.out.println("Please enter a sentence.");
        String sentence = input.nextLine();
        sentence = sentence.trim();
        int count = 0;
        for (int i = 0; i <sentence.length()-1 ; i++) {
            if(sentence.charAt(i)==' ') count++;
        }if (count<1) System.out.println("This sentence does not have 2 or more words to swap");
        else System.out.println(sentence.substring(sentence.lastIndexOf(' ')+1) +
                    sentence.substring(sentence.indexOf(' '), sentence.lastIndexOf(' ')+1) +
                    sentence.substring(0, sentence.indexOf(' ')));

        System.out.println("\n--------------------Task3------------------------");

        String str1 = "These books are so stupid";
        String str2 = "I like idiot behaviors";
        String str3 = "I had some stupid t-shirts in the past and also some idiot look shoes";

        str1 = ScannerHelper.getAString();

        if (str1.contains("idiot")||str1.contains("stupid")){
           str1 =  str1.replaceAll("idiot", "nice");
           str1 = str1.replaceAll("stupid", "nice");}
        System.out.println(str1);

        System.out.println("\n--------------------Task4------------------------");

        String name = ScannerHelper.getAName();
        if(name.length()<2){System.out.println("Invalid input!!!");}
        else if (name.length()%2==0){ System.out.println(name.substring(name.length()/2-1, name.length()/2+1));}
            else System.out.println(name.charAt(name.length()/2));

        System.out.println("\n--------------------Task5------------------------");

        System.out.println("Please enter a country");
        String country = input.nextLine();
        if (country.length()<6) System.out.println("Invalid input!!!");
        else System.out.println(country.substring(2, country.length()-2));

        System.out.println("\n--------------------Task6------------------------");

        String address = ScannerHelper.getAnAddress();
        String lower = address.toLowerCase();
        String replace = "";
        for (int i = 0; i < address.length(); i++) {
            if (lower.charAt(i)=='a') replace = replace +  '*';
            else if (lower.charAt(i)=='e') replace = replace +  '#';
            else if (lower.charAt(i)=='i') replace = replace +  '+';
            else if (lower.charAt(i)=='o') replace = replace +  '$';
            else if (lower.charAt(i)=='u') replace = replace +  '@';
            else replace = replace + address.charAt(i);
        }
        System.out.println(replace);

        System.out.println("\n--------------------Task7------------------------");

        int ran1 = RandomNumberGenerator.getARandomNumber(0,25);
        int ran2 = RandomNumberGenerator.getARandomNumber(0,25);
        String ascend = "";
        for (int i = Math.min(ran1, ran2); i < Math.max(ran1, ran2); i++) {
            if(i%5==0)continue;
            else ascend = ascend + i + " - ";
        }
        System.out.println(ascend.substring(0,ascend.length()-3));

        System.out.println("\n--------------------Task8------------------------");

        System.out.println("Please enter a sentence");
        String sentence1 = input.nextLine();
        sentence1 = sentence1.trim();
        int count2 = 1;
        for (int i = 0; i <sentence1.length()-1 ; i++) {
        if(sentence1.charAt(i)==' ' && sentence1.charAt(i-1)!=' ') count2++;}
        if(count2==1) System.out.println("This sentence does not have multiple words");
        else System.out.println("This sentence has " + count2 + " words.");

        System.out.println("\n--------------------Task9------------------------");

        System.out.println("Please enter a positive number");
        int positive = input.nextInt();
        input.nextLine();
        for (int i = 1; i <= positive ; i++) {
            if (i%6==0){ System.out.println("FooBar");}
           else if (i%2==0){ System.out.println("Foo");}
            else if (i%3==0){ System.out.println("Bar");}
            else System.out.println(i);}

            System.out.println("\n--------------------Task10-----------------------");

            String word = ScannerHelper.getAString();
            String pal = "";

            for (int i = 0; i < word.length(); i++) {
            if(word.isEmpty()) {System.out.println("This word does not have 1 or more characters");}
            else  pal = pal + word.charAt(word.length()-1-i);}

            if (pal.equals(word)) {System.out.println("This word is palindrome");}
            else System.out.println("This word is not palindrome");

        System.out.println("\n--------------------Task11-----------------------");

        String howManyAs = ScannerHelper.getAString();
        howManyAs = howManyAs.toLowerCase();
        int count1 = 0;
        for (int i = 0; i < howManyAs.length(); i++) {
            if (howManyAs.charAt(i)=='a'){ count1++;}
        }
        System.out.println("This sentence has " + count1 + " a or A letters.");

        System.out.println("\n--------------------EndofTasks-----------------------");


    }
}
