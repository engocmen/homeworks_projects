package projects;

import javax.xml.stream.events.Characters;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Project06 {
static Pattern pattern = Pattern.compile(" [a-zA-Z]{2,10}");
static Matcher matcher = pattern.matcher("");


  //      System.out.println("\n--------------------Task1-----------------------");

        public static int countMultipleWords(String[] words) {
                  int count = 0;
                for (String word : words) {
                        for (int i = 0; i < word.trim().length(); i++) {
                                if (word.trim().charAt(i) == ' ' && word.trim().charAt(i+1) != ' '){
                                    count++;
                                    break;
                                }
                        }}
                return count;}

    //      System.out.println("\n--------------------Task2-----------------------");

    public static ArrayList<Integer> removeNegatives(ArrayList<Integer> numbers) {
        for (int i = 0; i < numbers.size(); i++) {
            if (numbers.get(i)<0){
                numbers.remove(i);
                i--;}}
        return numbers;}

    //      System.out.println("\n--------------------Task3-----------------------");

    public static boolean validatePassword(String password){
        return (Pattern.matches("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&-+=()])(?=\\S+$).{8,20}$", password));

    }

    //      System.out.println("\n--------------------Task4-----------------------");

    public static boolean validateEmailAddress(String email){


    return (Pattern.matches("[\\w\\d]{2,}@[\\w\\d]{2,}.[\\w\\d]{2,}", email));}







        }
